//
//  UIColor+random.h
//  Task2
//
//  Created by obozhdi on 07/01/16.
//  Copyright © 2016 obozhdi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (random)

+ (UIColor *)randomColor;

@end
