//
//  UIColor+random.m
//  Task2
//
//  Created by obozhdi on 07/01/16.
//  Copyright © 2016 obozhdi. All rights reserved.
//

#import "UIColor+random.h"

@implementation UIColor (random)

+ (UIColor *)randomColor
{
    float r = arc4random_uniform(255) / 255.0f;
    float g = arc4random_uniform(255) / 255.0f;
    float b = arc4random_uniform(255) / 255.0f;
    float a = arc4random_uniform(100) / 100.0f;
    
    return [UIColor colorWithRed:r green:g blue:b alpha:a];
}

@end
