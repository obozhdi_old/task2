//
//  ScrollerView.m
//  Task2
//
//  Created by obozhdi on 07/01/16.
//  Copyright © 2016 obozhdi. All rights reserved.
//

#import "ScrollerView.h"

@interface ScrollerView ()

@property (nonatomic, strong) UISegmentedControl *segmentedControl;

@end

@implementation ScrollerView

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        
        self.segmentedControl            = [[UISegmentedControl alloc] initWithItems: @[@"first", @"second"]];
        [self addSubview:self.segmentedControl];
    }
    
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    static UIEdgeInsets const insets = {8.0f, 22.0f, 8.0f, 22.0f};

    self.segmentedControl.frame      = UIEdgeInsetsInsetRect(self.bounds, insets);
}

@end
