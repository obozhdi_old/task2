//
//  MainViewController.m
//  Task2
//
//  Created by obozhdi on 06/01/16.
//  Copyright © 2016 obozhdi. All rights reserved.
//

#import "MainViewController.h"
#import "ScrollerView.h"

typedef NS_ENUM(NSInteger, ScrollDirection) {
    ScrollDirectionTop,
    ScrollDirectionBottom
};


@interface MainViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, assign) ScrollDirection scrollDirection;
@property (nonatomic, assign) CGFloat lastYoffset;
@property (nonatomic, assign) BOOL isScrollingToTop;

@end

@implementation MainViewController

static NSString * const cellReuseIdentifier        = @"TableViewCell";
static CGFloat    const sectionHeaderHeight        = 44.0f;
static NSString * const tableHeaderReuseIdentifier = @"ScrollerView";

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.edgesForExtendedLayout               = UIRectEdgeNone;
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0,
                                                                   0,
                                                                   CGRectGetWidth(self.view.bounds),
                                                                   CGRectGetHeight(self.view.bounds) - CGRectGetMaxY(self.navigationController.navigationBar.frame))
                                                  style:UITableViewStylePlain];
    self.tableView.backgroundColor = [UIColor whiteColor];
    
    self.tableView.delegate   = self;
    self.tableView.dataSource = self;
    
    [self.tableView registerClass: [UITableViewCell class]  forCellReuseIdentifier:cellReuseIdentifier];
    [self.tableView registerClass: [ScrollerView class]     forHeaderFooterViewReuseIdentifier:tableHeaderReuseIdentifier];

    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0,
                                                                           0,
                                                                           CGRectGetWidth(self.view.bounds),
                                                                           sectionHeaderHeight)];
    searchBar.userInteractionEnabled = NO;
    self.tableView.tableHeaderView   = searchBar;
    
    [self.view addSubview:self.tableView];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)aSection {return 30;}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)aSection
{
    return [tableView dequeueReusableHeaderFooterViewWithIdentifier:tableHeaderReuseIdentifier];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {return 120.0f;}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)anIndexPath
{
    UITableViewCell *cell            = [tableView dequeueReusableCellWithIdentifier:cellReuseIdentifier forIndexPath:anIndexPath];
    cell.textLabel.text              = [NSString stringWithFormat:@"%li", (long)anIndexPath.row];
    cell.backgroundColor             = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor randomColor];

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)aSection {return sectionHeaderHeight;}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.isScrollingToTop) {
        if (scrollView.contentOffset.y <= sectionHeaderHeight) {
            scrollView.contentInset = UIEdgeInsetsZero;
            self.isScrollingToTop   = NO;
        }
        return;
    }
    if (scrollView.contentOffset.y < scrollView.contentSize.height - CGRectGetHeight(scrollView.frame) &&
        scrollView.contentOffset.y > sectionHeaderHeight) {
        if (self.lastYoffset < scrollView.contentOffset.y) {
            self.scrollDirection   = ScrollDirectionBottom;
            UIEdgeInsets theInsets = scrollView.contentInset;
            
            theInsets.top          -= scrollView.contentOffset.y - self.lastYoffset;
            
            if (theInsets.top < sectionHeaderHeight * -1) {
                theInsets.top = sectionHeaderHeight * -1;
            }
            
            scrollView.contentInset = theInsets;
        } else {
            self.scrollDirection   = ScrollDirectionTop;
            UIEdgeInsets theInsets = scrollView.contentInset;
            
            theInsets.top          += self.lastYoffset - scrollView.contentOffset.y;
            
            if (theInsets.top > 0) {
                theInsets.top = 0;
            }
            
            scrollView.contentInset = theInsets;
        }
    }
    self.lastYoffset = scrollView.contentOffset.y;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    UIEdgeInsets theInsets = scrollView.contentInset;
    CGPoint theOffset      = scrollView.contentOffset;
    
    if (self.scrollDirection == ScrollDirectionTop) {
        theInsets.top = 0;
        
        [UIView animateWithDuration:0.25f
                         animations:^
         {
             scrollView.contentInset = theInsets;
         }];
    } else {
        theOffset.y += sectionHeaderHeight + theInsets.top;
        if (theOffset.y > scrollView.contentSize.height - scrollView.frame.size.height) {
            theInsets.top = sectionHeaderHeight * -1;
            [UIView animateWithDuration:0.25f
                             animations:^
             {
                 scrollView.contentInset = theInsets;
             }];
        } else {
            [scrollView setContentOffset:theOffset animated:YES];
        }
    }
}

- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView
{
    self.isScrollingToTop = YES;
    [scrollView setContentOffset:CGPointZero animated:YES];
    
    return NO;
}

@end
